FROM docker.io/library/ruby:alpine3.17 AS base

ENV RAILS_ROOT=/usr/src/app
ENV RAILS_ENV=production
ENV NODE_ENV=production
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_SERVE_STATIC_FILES=true
ENV PORT=3000

ARG VERSION_TAG
ENV VERSION_TAG=$VERSION_TAG

RUN apk add -U git bash curl jq
ADD get_latest_release.sh /usr/local/bin/get_latest_release
RUN /usr/local/bin/get_latest_release

ENV PATH=$PATH:$RAILS_ROOT/bin
WORKDIR $RAILS_ROOT
RUN bundle config --local deployment 'true' \
    && bundle config --local without 'development:test'

FROM base as build

ARG PACKAGES='alpine-sdk libpq-dev'
RUN apk update \
    && apk add --update --no-cache ${PACKAGES} \
    && bundle install --no-cache \
    && bundle doctor

FROM base as prod

ARG PACKAGES='libpq-dev tzdata imagemagick yarn bash'
COPY --from=build $RAILS_ROOT/vendor/bundle ./vendor/bundle

RUN apk update \
    && apk add --update --no-cache ${PACKAGES} \
    && yarn install --production --frozen-lockfile \
    && yarn cache clean

RUN apk update \
    && apk upgrade \
    && update-ca-certificates

EXPOSE ${PORT}
ENTRYPOINT [ "./bin/start" ]
