#!/bin/bash

set -x

repo=bigbluebutton/greenlight
api_url="https://api.github.com/repos/$repo/releases/latest"

latest_release=$(curl -s "$api_url" | jq -r '.tag_name')
clone_url="https://github.com/$repo.git"

git clone "$clone_url" /usr/src/app

cd /usr/src/app
git checkout "$latest_release"
